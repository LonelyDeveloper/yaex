package com.LonelyDeveloper.YaEx.database_management;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private ArrayList<Element> elements;

    public DBHelper(Context context) {
        super(context, "database", null, 1);
        updateElements();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table treeTable (id integer primary key,name text,parent integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public ArrayList<Element> getElements() {
        return elements;
    }

    public void updateElements() {
        elements = new ArrayList<>();
        Cursor c = this.getWritableDatabase().query("treeTable", null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");
            int parentColIndex = c.getColumnIndex("parent");
            do {
                elements.add(new Element(c.getString(nameColIndex), c.getInt(idColIndex),
                        c.getInt(parentColIndex), false));
            } while (c.moveToNext());
        }
        c.close();
    }

    public void writeInDB(ArrayList<Element> elementsToWrite, boolean clearTable) {
        if (clearTable)
            clearTreeTable();
        ContentValues contentValues = new ContentValues();
        for (Element element : elementsToWrite) {
            dbInsertion(element.getId(), element.getName(), element.getParentId(), contentValues);
        }
        elements = elementsToWrite;
    }

    public void clearTreeTable() {
        this.getWritableDatabase().delete("treeTable", null, null);
    }

    private void dbInsertion(int id, String name, int parentId, ContentValues contentValues) {
        contentValues.put("id", id);
        contentValues.put("name", name);
        contentValues.put("parent", parentId);
        this.getWritableDatabase().insert("treeTable", null, contentValues);
    }
}