package com.LonelyDeveloper.YaEx.database_management;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class UpdateTableTask extends AsyncTask<URL, String, Void> {
    private final DBHelper dbHelper;
    private final Context context;
    private  Exception exception;

    public UpdateTableTask(DBHelper dbHelper, Context context) {
        this.dbHelper = dbHelper;
        this.context = context;
    }

    @Override
    protected Void doInBackground(URL... urls) {
        for (URL urlWeb : urls) {
            try {
                boolean clearTable = urlWeb == urls[0];
                dbHelper.writeInDB(ParserForDatabase.parseToElementsForDb(webSiteContent(urlWeb)), clearTable);
            } catch (JSONException | IOException e) {
                exception = e;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (exception != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Error")
                    .setMessage(exception.getMessage())
                    .setCancelable(false)
                    .setNegativeButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private String webSiteContent(URL urlWeb) throws IOException {
        StringBuilder websiteContent = null;
        InputStream inputStream = null;
        BufferedReader bufferReader = null;
        try {
            inputStream = urlWeb.openStream();
            bufferReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            websiteContent = new StringBuilder();
            while ((inputLine = bufferReader.readLine()) != null) {
                websiteContent.append(inputLine);
            }
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
                if (bufferReader != null)
                    bufferReader.close();
            } catch (IOException ignore) {
            }
        }
        return websiteContent.toString();
    }
}
