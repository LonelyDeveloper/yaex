package com.LonelyDeveloper.YaEx.database_management;

public class Element {
    private final String name;
    private final int parentId;
    private final int id;
    private boolean checked;

    public Element(String name, int id, int parentId, boolean checked) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public int getParentId() {
        return parentId;
    }

    public int getId() {
        return id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void checkedChanged() {
        checked = !checked;
    }
}
