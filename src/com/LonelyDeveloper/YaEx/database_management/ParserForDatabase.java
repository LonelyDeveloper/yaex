package com.LonelyDeveloper.YaEx.database_management;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParserForDatabase {
    private static int primaryKeyForDatabaseCounter;

    public static ArrayList<Element> parseToElementsForDb(String content) throws JSONException {
        primaryKeyForDatabaseCounter = 0;
        ArrayList<Element> elementsForDb = new ArrayList<>();
        JSONArray jsonContent = new JSONArray(content);
        parseJSON(elementsForDb, jsonContent, -1);
        return elementsForDb;
    }

    static void parseJSON(ArrayList<Element> elements, JSONArray jsonArray, int parentIdForDatabase) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject titleObject = jsonArray.getJSONObject(i);
            String name = titleObject.getString("title");
            int id = titleObject.has("id") ? titleObject.getInt("id") : primaryKeyForDatabaseCounter++;
            if (titleObject.has("subs")) {
                JSONArray subObjects = titleObject.getJSONArray("subs");
                parseJSON(elements, subObjects, id);
            }
            elements.add(new Element(name, id, parentIdForDatabase, false));
        }
    }
}
