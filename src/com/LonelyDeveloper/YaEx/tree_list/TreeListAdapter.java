package com.LonelyDeveloper.YaEx.tree_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.LonelyDeveloper.YaEx.database_management.Element;
import com.LonelyDeveloper.YaEx.R;

import java.util.ArrayList;

public class TreeListAdapter extends BaseAdapter {
    private TreeListView parentTreeListView;
    private Context context;
    private LayoutInflater lInflater;
    private ArrayList<Element> subElements;
    private ArrayList<Element> elements;

    public TreeListAdapter(Context context, ArrayList<Element> elements, int parent) {
        this.context = context;
        this.elements = elements;
        setSubElements(parent);
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    void setParentTreeListView(TreeListView parentTreeListView) {
        this.parentTreeListView = parentTreeListView;
    }

    private void setSubElements(int parent) {
        ArrayList<Element> temp = new ArrayList<>();
        for (Element e : elements) {
            if (e.getParentId() == parent)
                temp.add(e);
        }
        this.subElements = temp;
    }

    @Override
    public int getCount() {
        return subElements.size();
    }

    @Override
    public Element getItem(int position) {
        return subElements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Element element;
        Button btnShow;
        TreeListView SubListView;
        View view;
        view = convertView;
        if (view == null)
            view = lInflater.inflate(R.layout.item, parent, false);
        element = getItem(position);
        btnShow = (Button) view.findViewById(R.id.cbBox);
        btnShow.setVisibility(hasChildren(element) ? View.VISIBLE : View.GONE);
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                element.checkedChanged();
                updateMainListView();
            }
        });
        SubListView = ((TreeListView) view.findViewById(R.id.lvSub));
        if (element.isChecked()) {
            TreeListAdapter sub = new TreeListAdapter(context, elements, element.getId());
            sub.setParentTreeListView(parentTreeListView);
            SubListView.setAdapter(sub);
            btnShow.setText("Close");
        } else {
            SubListView.setAdapter(null);
            btnShow.setText("Show");
        }
        setSubListViewHeightBasedOnChildren(SubListView);
        ((TextView) view.findViewById(R.id.tvName)).setText(element.getName());
        return view;
    }

    private boolean hasChildren(Element element) {
        for (Element e : elements) {
            if (e.getParentId() == element.getId())
                return true;
        }
        return false;
    }

    private void setSubListViewHeightBasedOnChildren(ListView listView) {
        TreeListAdapter listAdapter = (TreeListAdapter) listView.getAdapter();
        if (listAdapter == null) {
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = listView.getDividerHeight();
            listView.setLayoutParams(params);
            listView.requestLayout();
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void updateMainListView() {
        parentTreeListView.update();
    }
}
