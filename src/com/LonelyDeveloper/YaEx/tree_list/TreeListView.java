package com.LonelyDeveloper.YaEx.tree_list;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class TreeListView extends ListView {
    private TreeListAdapter mainListAdapter;

    public TreeListView(Context context) {
        super(context);
    }

    public TreeListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TreeListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setMainListAdapter(TreeListAdapter adapter) {
        mainListAdapter = adapter;
        mainListAdapter.setParentTreeListView(this);
        setAdapter(mainListAdapter);
    }

    public void update() {
        mainListAdapter.notifyDataSetChanged();
    }
}
