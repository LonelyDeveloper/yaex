package com.LonelyDeveloper.YaEx;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.LonelyDeveloper.YaEx.database_management.DBHelper;
import com.LonelyDeveloper.YaEx.database_management.UpdateTableTask;
import com.LonelyDeveloper.YaEx.tree_list.TreeListAdapter;
import com.LonelyDeveloper.YaEx.tree_list.TreeListView;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity implements OnClickListener {
    final static String LOG_TAG = "myLogs";
    private DBHelper dbHelper;
    private TreeListView treeListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button updateBtn;
        setContentView(R.layout.main);
        dbHelper = new DBHelper(this);
        updateBtn = (Button) findViewById(R.id.updateBtn);
        treeListView = (TreeListView) findViewById(R.id.lvMain);
        updateBtn.setOnClickListener(this);
        setListViewAdapter(treeListView);
    }

    public void onClick(View v) {
        try {
            URL url = new URL("https://money.yandex.ru/api/categories-list");
            new UpdateTableTask(dbHelper, this).execute(url);
            setListViewAdapter(treeListView);
        } catch (MalformedURLException e) {
            Log.d(LOG_TAG, e.getMessage());
        }
    }

    private void setListViewAdapter(TreeListView treeListView){
        TreeListAdapter mainTreeListAdapter = new TreeListAdapter(this, dbHelper.getElements(), -1);
        treeListView.setMainListAdapter(mainTreeListAdapter);
    }
}
